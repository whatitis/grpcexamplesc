// googletest.cpp : 定义控制台应用程序的入口点。
//
#include "stdafx.h"
//#include <gtest\gtest.h>
//#include <gmock\gmock.h>

#include <iostream>

using namespace std;


// GLOG_NO_ABBREVIATED_SEVERITIES宏需要在logging.h、Windows.h之前定义，解决冲突


//#include <Windows.h>
#define GLOG_NO_ABBREVIATED_SEVERITIES

#include "glog\logging.h"
#include <gtest/gtest.h>



class MyEnvironment : public testing::Environment
{
public:
	virtual void SetUp()
	{
		std::cout << "Foo FooEnvironment SetUP" << std::endl;
	}
	virtual void TearDown()
	{
		std::cout << "Foo FooEnvironment TearDown" << std::endl;
		getchar();
	}
};


int Foo(int a, int b)

{
	//cin.get();
	if (a == 0 || b == 0)

	{

		throw "don't do that";

	}

	int c = a % b;

	if (c == 0)

		return b;

	return Foo(b, c);

}





TEST(FooTest, HandleNoneZeroInput)

{

	EXPECT_EQ(2, Foo(4, 10));

	EXPECT_EQ(6, Foo(30, 18));

	cin.get();

}

int _tmain(int argc, _TCHAR* argv[])
{
	// 设置日志消息除了日志文件之外是否去标准输出
	FLAGS_alsologtostderr = false;

	// 严重性级别在该门限值以上的日志信息除了写入日志文件以外，还要输出到stderr。
	// 各严重性级别对应的数值：INFO—0，WARNING—1，ERROR—2，FATAL—3	默认值为2.
	FLAGS_stderrthreshold = 0;

	// 严重性级别在该门限值以上的日志信息才进行记录。	默认值为0.
	FLAGS_minloglevel = 0;

	// 值为true的时候，日志信息输出到stderr，并非文件。默认值为 false。
	FLAGS_logtostderr = false;

	// 设置记录到标准输出的颜色消息（如果终端支持）
	FLAGS_colorlogtostderr = true;
	// 设置日志前缀是否应该添加到每行输出
	FLAGS_log_prefix = true;
	// 设置可以缓冲日志的最大秒数，0指实时输出
	FLAGS_logbufsecs = 0;
	// 设置最大日志文件大小（以MB为单位）
	FLAGS_max_log_size = 128;
	// 设置是否在磁盘已满时避免日志记录到磁盘
	FLAGS_stop_logging_if_full_disk = true;

	//FLAGS_log_dir = "./logging.txt"; 这个设置4.0好像不生效

	// 指定级别日志输出目录
	google::SetLogDestination(google::GLOG_INFO, "./INFO/");
	// 创建INFO目录
	_mkdir("./INFO");


	// 指定日志级别的前缀
	google::SetLogDestination(google::GLOG_WARNING, "./warning");

	// 所以日志级别文件前缀
	google::SetLogFilenameExtension("log");

	// ###############################
	// 初始化glog，参数为程序名
	google::InitGoogleLogging("test");

	// ################################

	//FLAGS_log_dir = "./";

	// glog有四个错误级别

	LOG(INFO) << "I am INFO ";
	LOG(WARNING) << "I am Warning ";
	LOG(ERROR) << "I am Error ";
	// FATAL：致命错误，会停止程序，并报出代码段信息。
	//LOG(FATAL) << "I am Fatal";


	//cin.get();
	testing::AddGlobalTestEnvironment(new MyEnvironment);
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}


//int main(int argc , char **argv)
//{
//	testing::InitGoogleTest(&argc,argv);
//	//testing::InitGoogleTest();
//	RUN_ALL_TESTS();
//	getchar();
//	int a = 10, b = 20;
//	//ASSERT_EQ(a== b);
//	//ASSERT_EQ(10, 10);
//
//	cin.get();
//    return RUN_ALL_TESTS();
//}
//
//TEST(test, first)
//{
//	EXPECT_EQ(10, 10);
//
//}