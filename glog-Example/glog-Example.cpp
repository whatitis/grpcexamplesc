// glog-Example.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"


#include <iostream>
#include <fstream>
#include <direct.h>
using namespace std;

#define GOOGLE_GLOG_DLL_DECL
// GLOG_NO_ABBREVIATED_SEVERITIES宏需要在logging.h、Windows.h之前定义，解决冲突
#define GLOG_NO_ABBREVIATED_SEVERITIES

//#include <Windows.h>


//#include "glog\logging.h"
#include <glog\logging.h>



int main(unsigned int args, char *argv[])
{
	
	// 设置日志消息除了日志文件之外是否去标准输出
	//FLAGS_alsologtostderr = false;

	//// 严重性级别在该门限值以上的日志信息除了写入日志文件以外，还要输出到stderr。
	//// 各严重性级别对应的数值：INFO—0，WARNING—1，ERROR—2，FATAL—3	默认值为2.
	//FLAGS_stderrthreshold = 0;

	//// 严重性级别在该门限值以上的日志信息才进行记录。	默认值为0.
	//FLAGS_minloglevel = 0;

	//// 值为true的时候，日志信息输出到stderr，并非文件。默认值为 false。
	//FLAGS_logtostderr = false;

	//// 设置记录到标准输出的颜色消息（如果终端支持）
	//FLAGS_colorlogtostderr = true;
	//// 设置日志前缀是否应该添加到每行输出
	//FLAGS_log_prefix = true;
	//// 设置可以缓冲日志的最大秒数，0指实时输出
	//FLAGS_logbufsecs = 0;
	//// 设置最大日志文件大小（以MB为单位）
	//FLAGS_max_log_size = 128;
	//// 设置是否在磁盘已满时避免日志记录到磁盘
	//FLAGS_stop_logging_if_full_disk = true;

	//FLAGS_log_dir = "./logging.txt"; 这个设置4.0好像不生效

	// 指定级别日志输出目录
	//google::SetLogDestination(google::GLOG_INFO, "./INFO/");
	//// 创建INFO目录
	//_mkdir("./INFO");


	//// 指定日志级别的前缀
	//google::SetLogDestination(google::GLOG_WARNING, "./warning");

	//// 所以日志级别文件前缀
	//google::SetLogFilenameExtension("log");

	//// ###############################
	//// 初始化glog，参数为程序名
	google::InitGoogleLogging(argv[0]);
	//LOG(google::GLOG_WARNING) << "hello";
	 //LOG(INFO) << "hello";

	LOG(INFO) << "hello";
	// ################################

	//FLAGS_log_dir = "./";

	// glog有四个错误级别

	//LOG(INFO) << "I am INFO ";
	//LOG(WARNING) << "I am Warning ";
	//LOG(ERROR) << "I am Error ";
	// FATAL：致命错误，会停止程序，并报出代码段信息。
	//LOG(FATAL) << "I am Fatal";

	// ###########################
	// ####### 条件输出 ##########

	//当条件满足时输出日志
	//LOG_IF(INFO, 20 > 10) << "Got lots of cookies";

	//// google::COUNTER 记录该语句被执行次数，从1开始，在第一次运行输出日志之后，每隔 10 次再输出一次日志信息
	//for (size_t i = 0; i < 30; i++)
	//{
	//	LOG_EVERY_N(INFO, 10) << "Got the " << google::COUNTER << "th cookie";

	//}

	////上述两者的结合，不过要注意，是先每隔 10 次去判断条件是否满足，如果是则输出一条日志；而不是当满足某条件的情况下，每隔 10 次输出一次日志信息
	//for (size_t i = 0; i < 30; i++)
	//{
	//	LOG_IF_EVERY_N(INFO, (i % 2 == 0), 10) << "Got the " << google::COUNTER << "th % cookie";

	//}

	////当此语句执行的前 5 次都输出日志，然后不再输出
	//for (size_t i = 0; i < 30; i++)
	//{
	//	LOG_FIRST_N(INFO, 5) << "Got the " << google::COUNTER << "th cookie";
	//}

	// ############################
	// 关闭glog
	google::ShutdownGoogleLogging();

	// ############################
	//getchar();
	return 0;
}
